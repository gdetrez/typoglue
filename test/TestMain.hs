import Test.Tasty
import Test.Tasty.HUnit
-- import Test.Tasty.QuickCheck
import Unlexers
import Spaces

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Typographic conventions"
    [ testGroup "English" $
        let sut = render spaceToHtml . unEnglish in
        [ testCase "comma" $
            sut ["foo", ",", "bar"] @?= "foo, bar"
        , testCase "semicolon" $
            sut ["foo", ";", "bar"] @?= "foo; bar"
        , testCase "colon" $
            sut ["foo", ":", "bar"] @?= "foo: bar"
        , testCase "period" $
            sut ["foo", ".", "bar"] @?= "foo. bar"
        , testCase "exclamation mark" $
            sut ["foo", "!", "bar"] @?= "foo! bar"
        , testCase "interrogation mark" $
            sut ["foo", "?", "bar"] @?= "foo? bar"
        , testCase "brackets" $
            sut ["foo", "(", "bar", ")", "foo"] @?= "foo (bar) foo"
        , testCase "single quotes" $
            sut ["foo", "‘", "bar", "’", "foo"] @?= "foo ‘bar’ foo"
        , testCase "double quotes" $
            sut ["foo", "“", "bar", "”", "foo"] @?= "foo “bar” foo"
        , testCase "bracket in quotes" $
            sut ["“", "(", "foo", ")", "”"] @?= "“(foo)”"
        , testCase "punctuation and brackets" $
            sut ["(", "foo", ".", ")", "?"] @?= "(foo.)?"
        ]
    , testGroup "French" $
        let sut = render spaceToHtml . unFrench in
        [ testCase "comma" $
            sut ["foo", ",", "bar"] @?= "foo, bar"
        , testCase "semicolon" $
            sut ["foo", ";", "bar"] @?= "foo&nbsp;; bar"
        , testCase "colon" $
            sut ["foo", ":", "bar"] @?= "foo&nbsp;: bar"
        , testCase "period" $
            sut ["foo", ".", "bar"] @?= "foo. bar"
        , testCase "exclamation mark" $
            sut ["foo", "!", "bar"] @?= "foo&nbsp;! bar"
        , testCase "interrogation mark" $
            sut ["foo", "?", "bar"] @?= "foo&nbsp;? bar"
        , testCase "brackets" $
            sut ["foo", "(", "bar", ")", "foo"] @?= "foo (bar) foo"
        , testCase "single quotes" $
            sut ["foo", "‘", "bar", "’", "foo"] @?= "foo ‘bar’ foo"
        , testCase "double quotes" $
            sut ["foo", "“", "bar", "”", "foo"] @?= "foo “bar” foo"
        , testCase "guillemets" $
            sut ["foo", "«", "bar", "»", "foo"] @?= "foo «&#x202F;bar&#x202F;» foo"
        , testCase "bracket in quotes" $
            sut ["“", "(", "foo", ")", "”"] @?= "“(foo)”"
        , testCase "punctuation and brackets" $
            sut ["(", "foo", ".", ")", "?"] @?= "(foo.)&nbsp;?"
        ]
    ]
