module Spaces where

-- | Different space characters available in unicode
-- Superseeded character en quad, em quad and zero width non-breaking space
-- have been excluded from this list.
-- See https://en.wikipedia.org/wiki/Whitespace_character
data Space =
    Space                   -- ^ Most common (normal ASCII space)
  | NoBreakSpace            -- ^ identical to Space, but not a point at which a
                            -- line may be broken.
  | OghamSpaceMark          -- ^ Used for interword separation in Ogham text.
  | EnSpace                 -- ^ Also known as "nut". Width of one en.
  | EmSpace                 -- ^ Also known as "mutton". Width of one em.
  | ThreePerEmSpace         -- ^ Also known as "thick space". One third of an
                            -- em wide.
  | FourPerEmSpace          -- ^ Also known as "mid space". One fourth of an em
                            -- wide.
  | SixPerEmSpace           -- ^ One sixth of an em wide.
  | FigureSpace             -- ^ Figure space. In fonts with monospaced digits,
                            -- equal to the width of one digit.
  | PunctuationSpace        -- ^ As wide as the narrow punctuation in a font,
                            -- i.e. the advance width of the period or comma.
  | ThinSpace               -- ^ One-fifth (sometimes one-sixth) of an em wide.
                            -- Recommended for use as a thousands separator for
                            -- measures made with SI units.
  | HairSpace               -- ^ Thinner than a thin space.
  | NarrowNoBreakSpace      -- ^ Similar in function to U+00A0 No-Break Space.
  | MediumMathematicalSpace -- ^ Used in mathematical formulae.
  | IdeographicSpace        -- ^ As wide as a CJK character cell.
  | MongolianVowelSeparator -- ^ A narrow space character, used in Mongolian to
                            -- cause the final two characters of a word to take
                            -- on different shapes.
  | ZeroWidthSpace          -- ^ Used to indicate word boundaries to text
                            -- processing systems when using scripts that do
                            -- not use explicit spacing.
  | ZeroWidthNonJoiner      -- ^ When placed between two characters that would
                            -- otherwise be connected, a ZWNJ causes them to be
                            -- printed in their final and initial forms,
  | ZeroWidthJoiner         -- ^ When placed between two characters that would
                            -- otherwise not be connected, a ZWJ causes them to
                            -- be printed in their connected forms.
  | WordJoiner              -- ^ Similar to ZeroWidthSpace, but not a point at
                            -- which a line may be broken.
  deriving (Show, Eq)

-- HTML-ish aliases
sp      = Space
nbsp    = NoBreakSpace
nnbsp   = NarrowNoBreakSpace
ensp    = EnSpace
emsp    = EmSpace
emsp13  = ThreePerEmSpace
emsp14  = FourPerEmSpace
puncsp  = PunctuationSpace
thinsp  = ThinSpace
hairsp  = HairSpace
zwnj    = ZeroWidthNonJoiner
zwj     = ZeroWidthJoiner
wj      = WordJoiner




spaceToUnicode  :: Space -> String
spaceToUnicode s = case s of
    Space                   -> "\x0020"
    NoBreakSpace            -> "\x00A0"
    OghamSpaceMark          -> "\x1680"
    EnSpace                 -> "\x2002"
    EmSpace                 -> "\x2003"
    ThreePerEmSpace         -> "\x2004"
    FourPerEmSpace          -> "\x2005"
    SixPerEmSpace           -> "\x2006"
    FigureSpace             -> "\x2007"
    PunctuationSpace        -> "\x2008"
    ThinSpace               -> "\x2009"
    HairSpace               -> "\x200A"
    NarrowNoBreakSpace      -> "\x202F"
    MediumMathematicalSpace -> "\x205F"
    IdeographicSpace        -> "\x3000"
    MongolianVowelSeparator -> "\x180E"
    ZeroWidthSpace          -> "\x200B"
    ZeroWidthNonJoiner      -> "\x200C"
    ZeroWidthJoiner         -> "\x200D"
    WordJoiner              -> "\x2060"

spaceToAscii    :: Space -> String
spaceToAscii s = case s of
    Space                   -> " "
    NoBreakSpace            -> " "
    OghamSpaceMark          -> " "
    EnSpace                 -> " "
    EmSpace                 -> " "
    ThreePerEmSpace         -> " "
    FourPerEmSpace          -> " "
    SixPerEmSpace           -> " "
    FigureSpace             -> " "
    PunctuationSpace        -> " "
    ThinSpace               -> " "
    HairSpace               -> " "
    NarrowNoBreakSpace      -> " "
    MediumMathematicalSpace -> " "
    IdeographicSpace        -> " "
    MongolianVowelSeparator -> ""
    ZeroWidthSpace          -> ""
    ZeroWidthNonJoiner      -> ""
    ZeroWidthJoiner         -> ""
    WordJoiner              -> ""

spaceToHtml     :: Space -> String
spaceToHtml s = case s of
    Space                   -> " "
    NoBreakSpace            -> "&nbsp;"
    OghamSpaceMark          -> "&#x1680;"
    EnSpace                 -> "&ensp;"
    EmSpace                 -> "&emsp;"
    ThreePerEmSpace         -> "&emsp13;"
    FourPerEmSpace          -> "&emsp14;"
    SixPerEmSpace           -> "&#x2006;"
    FigureSpace             -> "&#x2007;"
    PunctuationSpace        -> "&puncsp;"
    ThinSpace               -> "&thinsp;"
    HairSpace               -> "&hairsp;"
    NarrowNoBreakSpace      -> "&#x202F;"
    MediumMathematicalSpace -> "&#x205F;"
    IdeographicSpace        -> "&#x3000;"
    MongolianVowelSeparator -> "&#x180E;"
    ZeroWidthSpace          -> "&#x200B;"
    ZeroWidthNonJoiner      -> "&zwnj;"
    ZeroWidthJoiner         -> "&zwj;"
    WordJoiner              -> "&#x2060;"
