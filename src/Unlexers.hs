module Unlexers where

import Spaces


render :: (Space -> String) -> [Either Space String] -> String
render spaceRenderer = concatMap (either spaceRenderer id)

unEnglish :: [String] -> [Either Space String]
unEnglish (a:b:c) | b `elem` map (:[]) ",;:.!?)’”" = Right a:unEnglish (b:c)
unEnglish (a:b:c) | a `elem` map (:[]) "(‘“" = Right a:unEnglish (b:c)
unEnglish (a:b:c)= Right a:Left Space:unEnglish (b:c)
unEnglish [a] = [Right a]
unEnglish [] = []


unFrench :: [String] -> [Either Space String]
unFrench (a:b:c) | b `elem` map (:[]) ",.)’”" = Right a:unFrench (b:c)
unFrench (a:b:c) | b `elem` map (:[]) ";:!?" = Right a:Left nbsp:unFrench (b:c)
unFrench (a:b:c) | a `elem` map (:[]) "(‘“" = Right a:unFrench (b:c)
unFrench (a:b:c) | a == "\171" = Right a:Left nnbsp:unFrench (b:c)
unFrench (a:b:c) | b == "\187" = Right a:Left nnbsp:unFrench (b:c)
unFrench (a:b:c)= Right a:Left Space:unFrench (b:c)
unFrench [a] = [Right a]
unFrench [] = []
