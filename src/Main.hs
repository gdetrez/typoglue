{-# LANGUAGE RecordWildCards #-}
module Main where

import Data.Semigroup ((<>))

import Unlexers
import Spaces
import Options.Applicative

data Options = Options
  { unlexer :: [String] -> [Either Space String]
  , renderer :: Space -> String }

languages :: [(String, [String] -> [Either Space String])]
languages =
    [ ("french", unFrench)
    , ("english", unEnglish)
    ]


languageReader :: ReadM ([String] -> [Either Space String])
languageReader = do
    s <- str
    maybe (readerError "Unknown language") return $
        lookup s languages

formatReader :: ReadM (Space -> String)
formatReader = do
    s <- str
    case s of
        "unicode" -> return spaceToUnicode
        "ascii"   -> return spaceToAscii
        "html"    -> return spaceToHtml
        _         -> readerError $ "Unknown format " ++ s

options :: Parser Options
options = Options
  <$> option languageReader
      ( long "style"
     <> short 's'
     <> metavar "S"
     <> help "Spacing style: french or english" )
  <*> option formatReader
      ( long "encoding"
     <> short 'e'
     <> metavar "ENC"
     <> help "String encoding: unicode (default), ascii or html"
     <> value spaceToUnicode )

main :: IO ()
main = do
    opts <- execParser opts
    interact (unlines . map (typoGlue opts) . lines)
  where
    opts = info (helper <*> options)
      ( fullDesc
     <> progDesc "Print a greeting for TARGET"
     <> header "hello - a test for optparse-applicative" )
    typoGlue Options{..} = concatMap (either renderer id) . unlexer . words
    -- typoGlue = show . words

