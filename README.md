Typoglue
========

A small utility to glue tokens while respectinng punctuation spacing.

E.g.

    echo Hello , world ! | typoglue -s english
    Hello, world!

    echo Bonjour , monde ! | typoglue -s french 
    Bonjour, monde !
